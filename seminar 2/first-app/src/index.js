import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter, Route, Redirect} from "react-router-dom";

import App from "./components";
import Main from "./components/Main";
import Login from "./components/Login";

ReactDOM.render(<BrowserRouter>
    <Route exact path={"/"} component={App} />
    <Route path={"/ login"} component={Login} />
    <Route path={"/main"} component={Main} />
</BrowserRouter>, document.getElementById('root'));

