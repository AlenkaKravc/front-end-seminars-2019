import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeUserData} from "../../actions/user";
import Button from "../../components/Button";
import Input from "../../components/Input";
import styles from './sttyles.module.scss';

const form = [
    {
        key: "login",
        label: "Логин"
    },
    {
        key: "email",
        label: "E-mail"
    },
    {
        key: "first_name",
        label: "Имя"
    },
    {
        key: "last_name",
        label: "Фамилия"
    },
    {
        key: "group",
        label: "Группа"
    },
];

function UserSettings() {
    const {user} = useSelector(store => ({
        user: store.user
    }));
    const dispatch = useDispatch();
    const [data, changeData] = useState({
        login: user.login,
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
        group: user.group
    });

    const saveData = () => {
        dispatch(changeUserData(data));
        console.log(data);
    };

    return <div className={styles.setting_form}>
        <h1>Настрйоки</h1>
        {form.map((filed, key) =>
            <Input
                key={key}
                label={filed.label}
                data={data[filed.key]}
                changeData={new_value => changeData({...data, [filed.key]: new_value})}
            />)
        }

        <Button style={{marginTop: "20px"}} onClick={saveData}>Сохранить</Button>
    </div>;
}


export default UserSettings;