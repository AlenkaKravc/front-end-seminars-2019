import React from 'react';
import {withRouter} from "react-router-dom";
import Content from "../../components/Content";
import Header from "../../components/Header";
import UserInfo from "../../containers/UserInfo";
import UserSettings from "../../containers/UserSettings";
import styles from './styles.module.scss';

function HomePage({match}) {

    console.log({match});
    return <Content className={styles.left_content}>
        <Header>
            <UserInfo />
        </Header>
        <UserSettings />
    </Content>;
}


export default withRouter(HomePage);