import {C_USER, USER_STATUS} from "../constants";

export const user = (state = {}, action) => {
    switch (action.type) {
        case C_USER.ADD_USER: {
            return {...state, ...action.data, status: USER_STATUS.AUTH};
        }
        case C_USER.CHANGE_DATA: {
            return {...state, ...action.data}
        }
        case C_USER.REMOVE_USER: {
            return {
                login: "",
                email: "",
                avatar: null,
                status: USER_STATUS.NOT_AUTH,
                first_name: "",
                last_name: "",
                group: "",
            }
        }
        default:
            return state;
    }
};